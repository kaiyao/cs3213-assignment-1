import java.util.Collections;
import java.util.List;


public class ListSorter<T extends Comparable<T>> {
	
	public List<T> sort (List<T> list){
		Collections.sort(list);
		return list;
	}

}
