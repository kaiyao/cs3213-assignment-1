import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


public class ListLineWriter {
	
	private File file;
	private BufferedWriter buf;	
	
	public ListLineWriter(String fileName) throws IOException {
		file = new File(fileName);
	}
	
	public void write(List<Line> list) throws IOException{
		
		buf = new BufferedWriter(new FileWriter(file));
		
		for (Line line : list) {
			String str = line.toString();
			buf.write(capitalizeFirstCharacter(str));
			buf.newLine();
		}
		
		buf.close();
		
	}

	private String capitalizeFirstCharacter(String str) {
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}

}
