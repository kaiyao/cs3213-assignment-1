import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;


public class ListLineIgnorer {
	
	private HashSet<String> ignoreSet;
	
	public ListLineIgnorer(String ignoreListFileName) throws FileNotFoundException, IOException {
		
		List<String> ignoreList = new FileLineReader(ignoreListFileName).readFileAsLineList();
		ignoreSet = new HashSet<String>();
		for (String ignoreWord : ignoreList) {
			String strippedIgnoreWord = processWord(ignoreWord);
			ignoreSet.add(strippedIgnoreWord);
		}
		
	}
	
	public ListLineIgnorer(List<String> wordsToIgnore)  {
		
		ignoreSet = new HashSet<String>();
		for (String ignoreWord : wordsToIgnore) {
			String strippedIgnoreWord = processWord(ignoreWord);
			ignoreSet.add(strippedIgnoreWord.toLowerCase());
		}
		
	}
	
	public List<Line> ignore (List<Line> list){
		
		List<Line> processedList = new LinkedList<Line>();
		for (Line line : list) {
			
			String firstWord = line.getWordAt(0);
			String strippedFirstWord = processWord(firstWord);
			if (strippedFirstWord.isEmpty() || ignoreSet.contains(strippedFirstWord)) {
				// Word contains 
				// Do nothing
			}else{
				processedList.add(line);
			}
		}
		
		return processedList;
		
	}
	
	private String processWord(String word) {
		word = word.toLowerCase();
		word = word.replaceAll("\\W", "");
		return word;
	}

}
