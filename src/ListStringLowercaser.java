import java.util.LinkedList;
import java.util.List;


public class ListStringLowercaser {
	
	public List<String> lowercase(List<String> listOfStr){
		List<String> processedList = new LinkedList<String>();
		for(String str : listOfStr) {
			processedList.add(str.toLowerCase());
		}
		
		return processedList;
	}

}
