import java.awt.EventQueue;

import javax.swing.JFrame;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.border.EmptyBorder;


public class QueryUi {

	private JFrame frmKwicQuery;
	private JTextField textField;
	private JList<String> list;
	
	private static String filename;
	private static Path file;
	private JPanel panel;
	private JPanel panel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		if (args.length < 1) {
			System.out.println("File not specified");
			System.exit(1);
		}
		
		filename = args[0];
		file = new File(filename).toPath();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				try {
					QueryUi window = new QueryUi();
					window.frmKwicQuery.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public QueryUi() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKwicQuery = new JFrame();
		frmKwicQuery.setTitle("KWIC Query");
		frmKwicQuery.setBounds(100, 100, 450, 300);
		frmKwicQuery.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKwicQuery.getContentPane().setLayout(new BorderLayout(0, 0));
		
		panel = new JPanel();
		panel.setBorder(new EmptyBorder(5, 5, 0, 5));
		frmKwicQuery.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(5, 0));
		
		JLabel lblSearch = new JLabel("Search");
		panel.add(lblSearch, BorderLayout.WEST);
		
		textField = new JTextField();
		panel.add(textField);
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				
				String text = ((JTextField) arg0.getSource()).getText();
				
				System.out.println("Search for: "+ text);
				
				List<String> result = binarySearch(text);
				
				DefaultListModel<String> model = new DefaultListModel<String>();
				if (result != null) {
					for (String r : result) {
						model.addElement(r);
					}
					list.setModel(model);
				}
			}
		});
		textField.setColumns(30);
		
		panel_1 = new JPanel();
		panel_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmKwicQuery.getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		list = new JList<String>();
		panel_1.add(list);
	}

	private List<String> binarySearch(String text) {
		
		String matchText = text.replaceAll("[\\W&&[^ ]]", "");
		String currentLine = null;
		String previousLine = null;
		Long currentLinePos = 0L;
		
		try {
			
			FileChannel fc = FileChannel.open(file, StandardOpenOption.READ);
			long min = 0;
			long max = fc.size();
			
			ByteBuffer dst;
			long mid;
			int readChunkSize = 5;
			
			while (max > min) {
				
				String currentPosString = "";
				int currentReadChunkSize = 0;
				
				mid = (max - min)/2 + min;
				
				// TODO: handle start/end of file
				/*while (currentPosString.split("\n").length < 3){
					currentReadChunkSize += readChunkSize;
					dst = ByteBuffer.allocate(currentReadChunkSize);					
					fc.position(mid);
					fc.read(dst);
					currentPosString = new String(dst.array());
				}
				
				currentLine = currentPosString.split("\n")[1];*/
				
				// Find start of line from mid
				long currentLineStart = mid;
				dst = ByteBuffer.allocate(readChunkSize);
				fc.read(dst, currentLineStart);
				while (!(new String(dst.array())).startsWith("\n") && currentLineStart > 0) {
					currentLineStart--;
					dst = ByteBuffer.allocate(readChunkSize);
					fc.read(dst, currentLineStart);
				}
				
				// Because the last found character is "\n", we skip that character
				currentLineStart+=1;
				
				int currentLineLength = 0;
				dst = ByteBuffer.allocate(currentLineLength);
				fc.read(dst, currentLineStart);
				while (!(new String(dst.array())).endsWith("\n")) {
					currentLineLength++;
					dst = ByteBuffer.allocate(currentLineLength);
					fc.read(dst, currentLineStart);
				}
				
				currentLine = new String(dst.array());				
				System.out.println(currentLine);
				
				String matchCurrentLine = currentLine.replaceAll("[\\W&&[^ ]]", "");
				
				int compare = matchText.compareToIgnoreCase(matchCurrentLine);
				if (compare == 0) {
					//return currentLine;
					break;
				} else if (compare < 0) {
					max = currentLineStart-1;
				} else if (compare > 0) {
					min = currentLineStart + currentLineLength + 1;
				}
				
				/*if (currentLine.equals(previousLine)) {
					System.out.println();
					break;
				}*/
				
				previousLine = currentLine;
				
				currentLinePos = currentLineStart;
			}
			
			// Search backwards and forwards until chunk of matches found
			long currentLineStart = currentLinePos;
			dst = ByteBuffer.allocate(10000);
			if (currentLineStart - 5000 < 0) {
				currentLineStart = 0;
			}
			fc.read(dst, currentLineStart);
			String found = new String(dst.array());
			String[] foundArr = found.split("\n");
			List<String> filteredFound = new LinkedList<String>();
			for (int i = 0; i < foundArr.length; i++) {
				String f = foundArr[i].replaceAll("[\\W&&[^ ]]", "");
				if (f.toLowerCase().startsWith(matchText.toLowerCase())){
					filteredFound.add(foundArr[i]);
				}
			}
			
			return filteredFound;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			JOptionPane.showMessageDialog(frmKwicQuery, "The file specified could not be read.");
		}		
		
		return null;		
		
	}

}
