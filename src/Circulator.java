import java.util.LinkedList;
import java.util.List;


public class Circulator {
	
	public List<Line> circulate (List<Line> list){
		
		List<Line> processedList = new LinkedList<Line>();
		for (Line line : list) {
			
			for (int i = 0; i<line.size(); i++){
				line = line.rotateAntiClockwise();
				processedList.add(line);
			}
		}
		
		return processedList;
		
	}

}
