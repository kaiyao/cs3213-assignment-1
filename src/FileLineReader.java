import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class FileLineReader {
	
	private File file;
	private BufferedReader buf;	
	
	public FileLineReader(String fileName) throws FileNotFoundException {
		file = new File(fileName);
		buf = new BufferedReader(new FileReader(file));
	}
	
	public String readLine() throws IOException{
		return buf.readLine();
	}
	
	public List<String> readFileAsLineList() throws IOException{
		List<String> list = new LinkedList<String>();
		String line;
		while ((line = readLine()) != null) {
			list.add(line);
		}
		return list;
	}

}
