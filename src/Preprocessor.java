import java.util.LinkedList;
import java.util.List;


public class Preprocessor {

	public List<Line> process (List<String> listOfStrings) {
		
		List<Line> list = new LinkedList<Line>();
		for (String str : listOfStrings) {
			Line line = new Line(str);
			list.add(line);
		}
		
		return list;
		
	}
	
}
