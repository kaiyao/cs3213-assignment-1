import java.util.List;


public class ListLinePrinter {
	
	public static void printWithCapitalization(List<Line> list){
		
		for (Line line : list) {
			String str = line.toString();
			System.out.println(capitalizeFirstCharacter(str));
		}
		
	}

	private static String capitalizeFirstCharacter(String str) {
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}
	
}
