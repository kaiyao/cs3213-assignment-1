import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class FileLineBlockReader {
	
	private File file;
	private BufferedReader buf;	
	private int blockSize;
	
	public FileLineBlockReader(String fileName, int blockSize) throws FileNotFoundException {
		file = new File(fileName);
		buf = new BufferedReader(new FileReader(file));
		this.blockSize = blockSize;
	}
	
	public String readLine() throws IOException{
		return buf.readLine();
	}
	
	public List<String> readBlockAsLineList() throws IOException{
		List<String> list = new LinkedList<String>();
		String line;
		int linesRead = 0;
		
		while ((line = readLine()) != null) {
			list.add(line);
			linesRead++;
			
			if  (linesRead >= blockSize) {
				break;
			}
		}
		return list;
	}

}
