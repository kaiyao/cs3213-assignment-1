import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class KWIC {

	public static final int BLOCK_SIZE = 10000;

	public static void main(String[] args) {
		
		if (args.length < 3) {
			System.out.println("kwic phrasefile ignorewordfile outputfile");
			process("lines.txt", "ignore.txt", "output.txt");
		}else{
			String phraseListFileName = args[0];
			String ignoreWordListFileName = args[1];
			String outputFileName = args[2];
			
			long startTime = System.nanoTime();
			process(phraseListFileName, ignoreWordListFileName, outputFileName);
			long endTime = System.nanoTime();
			System.out.println("Time taken: "+ (endTime - startTime)/1000000000.0);
		
		}
		
	}
	
	public static void process(String phraseListFileName, String ignoreWordListFileName, String outputFileName){
		List<String> listOfStrings = null;
		List<String> listOfWordsToIgnore;
		
		//Load up ignore list
		ListLineIgnorer lineListIgnorer = null;
		try {
			listOfWordsToIgnore = new FileLineReader(ignoreWordListFileName).readFileAsLineList();
			lineListIgnorer = new ListLineIgnorer(listOfWordsToIgnore);
		} catch (FileNotFoundException e) {
			System.out.println("Ignore word list file cannot be found");
			System.out.println("Assuming no keywords to be ignored...");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Unable to read Ignore list file");
			System.out.println("Assuming no keywords to be ignored...");
			e.printStackTrace();
		}
		
		//Load up phrase list
		FileLineBlockReader lineReader;
		
		try {
			lineReader = new FileLineBlockReader(phraseListFileName, BLOCK_SIZE);
		} catch (IOException e) {
			System.out.println("Unable to read phrase list file");
			e.printStackTrace();
			return;
		}
		
		int blockCount = 1;
		
		try {
			
			listOfStrings = lineReader.readBlockAsLineList();
			while (listOfStrings != null && listOfStrings.size() > 0) {
				processBlock(lineListIgnorer, blockCount+".txt", listOfStrings);
				blockCount++;
				
				listOfStrings = lineReader.readBlockAsLineList();
				
				System.out.println(blockCount);
			}
			
			new BlockMerger().merge(blockCount, outputFileName);
		
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	}

	private static void processBlock(ListLineIgnorer lineListIgnorer,
			String outputFileName, List<String> listOfStrings) {
		
		List<Line> listOfLines;
		listOfLines = new Preprocessor().process(listOfStrings);
		listOfLines = new Circulator().circulate(listOfLines);		
		listOfLines = lineListIgnorer.ignore(listOfLines);		
		listOfLines = new ListSorter<Line>().sort(listOfLines);
		
		try {
			new ListLineWriter(outputFileName).write(listOfLines);			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
