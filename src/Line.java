public class Line implements Comparable<Line> {
	String[] phrase;

	@Override
	public int compareTo(Line other) {
		int diff = 0;
		for (int i = 0; i < this.phrase.length && i < other.phrase.length; i++) {
			diff = this.phrase[i].replaceAll("[\\W&&[^ ]]", "").toLowerCase().compareTo(
					other.phrase[i].replaceAll("[\\W&&[^ ]]", "").toLowerCase());
			if (diff == 0) {
				continue;
			} else {
				return diff;
			}
		}
		if (this.size() == other.size()) {
			return 0;
		} else if (this.size() < other.size()) {
			return 1;
		} else {
			return -1;
		}
	}

	public Line(String str) {
		phrase = str.split(" ");
	}

	public Line rotateAntiClockwise() {
		String[] rotated = new String[this.size()];
		System.arraycopy(this.phrase, 1, rotated, 0, this.size() - 1);
		rotated[this.size() - 1] = this.phrase[0];
		return new Line(rotated);
	}

	private Line(String[] phr) {
		phrase = phr;
	}

	public int size() {
		return phrase.length;
	}

	@Override
	public String toString() {
		String txt = "";
		for (int i = 0; i < phrase.length; i++) {
			txt += phrase[i];
			if (i < phrase.length - 1)
				txt += " ";
		}

		return txt;
	}

	/*
	public String getFirstWord() {
		if (phrase.length > 0){
			return phrase[0];
		}else{
			return null;
		}
	}
	*/
	
	public String getWordAt(int index) {
		if (index < phrase.length && index >= 0 && phrase.length > 0) {
			return phrase[index];
		} else {
			return null;
		}
	}

	public String getSubsequence(int start, int end) {
		if (start < 0 || end <= 0 || phrase.length == 0) {
			return null;
		}
		String subseq = "";
		for (int i = start; i < end; i++) {
			subseq += phrase[i];
			if (i < phrase.length - 1)
				subseq += " ";
		}
		return subseq;
	}

}
