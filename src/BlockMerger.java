import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;


public class BlockMerger {
	
	public void merge(int blockCount, String outputFileName) throws IOException{
		
		while (blockCount > 1) {
			int newBlockCount = 1;
			
			for (int i = 1; i <= blockCount; i+=2) {
				
				if (!(new File(i+".txt").exists())) {
					break;
				}
				
				if ((new File((i+1)+".txt").exists())) {

					BufferedReader in1 = new BufferedReader(new FileReader(i+".txt"));
					BufferedReader in2 = new BufferedReader(new FileReader((i+1)+".txt"));
					
					BufferedWriter out = new BufferedWriter(new FileWriter("merged.txt"));
					
					String line1;
					String line2;
					line1 = in1.readLine();
					line2 = in2.readLine();
					
					while (line1 != null && line2 != null){
					
						String line1compare = line1.replaceAll("[\\W&&[^ ]]", "");
						String line2compare = line2.replaceAll("[\\W&&[^ ]]", "");
						
						if (line1compare.compareToIgnoreCase(line2compare) < 0) {
							out.write(line1);
							line1 = in1.readLine();
						}else{
							out.write(line2);
							line2 = in2.readLine();
						}
						out.newLine();
						
					}
					
					while (line1 != null){					
						out.write(line1);
						out.newLine();
						line1 = in1.readLine();
					}
					
					while (line2 != null){					
						out.write(line2);
						out.newLine();
						line2 = in2.readLine();
					}
					
					in1.close();
					in2.close();
					out.close();
					
					Files.deleteIfExists(new File(i+".txt").toPath());
					Files.deleteIfExists(new File((i+1)+".txt").toPath());
					Files.move(new File("merged.txt").toPath(), new File(newBlockCount+".txt").toPath(), StandardCopyOption.ATOMIC_MOVE);
				
					System.out.println("Merged " + i + " with " + (i+1) + " into " + newBlockCount+".txt");
				
				}else{
					Files.move(new File(i+".txt").toPath(), new File(newBlockCount+".txt").toPath(), StandardCopyOption.ATOMIC_MOVE);
				
					System.out.println("Moved " + i + " into " + newBlockCount+".txt");
				}
				
				newBlockCount++;
			}
			
			blockCount = newBlockCount - 1;
			System.out.println("Starting new merge with " + blockCount);
			
		}
		
		Files.move(new File("1.txt").toPath(), new File(outputFileName).toPath(), StandardCopyOption.ATOMIC_MOVE);
	}

}
